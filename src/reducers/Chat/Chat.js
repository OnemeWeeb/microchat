import { ACTIONS } from '../../utils/constants';

const initialState = {
	messages: [
		// {
		// 	user: 'Admin',
		// 	text: 'Welcome',
		// 	date: (new Date()).getTime(),
		// },
		// {
		// 	user: 'Qwe',
		// 	text: 'hello',
		// 	date: (new Date()).getTime(),
		// },
		// {
		// 	user: 'Dude',
		// 	text: 'hi',
		// 	date: (new Date()).getTime(),
		// },
		// {
		// 	user: 'Dude',
		// 	text: 'dude',
		// 	date: (new Date()).getTime(),
		// },
	],
};

const chatReducer = (state = initialState, { type, payload }) => {
	switch (type) {
	case ACTIONS.FILL_MESSAGE_LIST:
		return {
			...state,
			messages: [
				...state.messages,
				payload,
			],
		};
	case ACTIONS.CLEAR_MESSAGE_LIST:
		return initialState;
	default:
		return state;
	}
};

export default chatReducer;