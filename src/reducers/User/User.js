import { ACTIONS } from '../../utils/constants';

const initialState = () => ({
	userInfo: {
		email: '',
		name: '',
		token: localStorage.getItem('token') || '',
	},
	authorized: false,
	status: '',
});

const userReducer = (state = initialState(), { type, payload }) => {
	switch (type) {
	case ACTIONS.FILL_USER_INFO:
		return {
			...state,
			userInfo: payload,
			authorized: true,
		};
	case ACTIONS.GET_STATUS_VALUE:
		return {
			...state,
			status: payload,
		};
	case ACTIONS.CLEAR_USER_INFO:
		localStorage.removeItem('token');
		return initialState();
	default:
		return state;
	}
};

export default userReducer;