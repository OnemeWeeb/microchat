import { combineReducers } from 'redux';
import userReducer from './User/User';
import chatReducer from './Chat/Chat';

const globalReducer = combineReducers({
	userState: userReducer,
	chatState: chatReducer,
});

export default globalReducer;