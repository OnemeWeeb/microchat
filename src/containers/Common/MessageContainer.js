import { connect } from 'react-redux';

import Message from '../../components/Common/Message/Message';

const mapStateToProps = (state, ownProps) => ({
	...state.chatState.messages[ownProps.index],
})

export default connect(mapStateToProps)(Message);