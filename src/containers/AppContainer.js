import { connect } from 'react-redux';

import App from '../components/App';
import { checkToken } from '../requests/user';

const mapDispatchToProps = {
	checkToken,
};

export default connect(null, mapDispatchToProps)(App);