import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Chat from '../../../components/Pages/Chat/Chat';

import { fillMessageList, clearMessageList } from '../../../actions/chat';
import { clearUserInfo, checkTokenAction } from '../../../actions/user';

const mapStateToProps = ({
	chatState: { messages },
	userState: { userInfo: { name } }
}) => ({ messages, name });

const mapDispatchToProps = {
	fillMessageList,
	clearMessageList,
	checkToken: checkTokenAction,
	clearUserInfo,
};

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withRouter,
)(Chat);