import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Login from '../../../components/Pages/Login/Login';

import { signInAction } from '../../../actions/user';

const mapDispatchToProps = {
	signIn: signInAction,
};

export default compose(
	connect(null, mapDispatchToProps),
	withRouter,
)(Login)