import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Registration from '../../../components/Pages/Registration/Registration';

import { signUpAction } from '../../../actions/user';

const mapDispatchToProps = {
	signUp: signUpAction,
};

export default compose(
	connect(null, mapDispatchToProps),
	withRouter,
)(Registration)