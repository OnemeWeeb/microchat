import { connect } from 'react-redux';
import AuthRoute from '../../components/Routes/AuthRoute/AuthRoute';

const mapStateToProps = ({
	userState: { userInfo: { token } },
}) => ({ token });

export default connect(mapStateToProps)(AuthRoute);