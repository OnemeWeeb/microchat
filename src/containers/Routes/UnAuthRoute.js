import { connect } from 'react-redux';
import UnAuthRoute from '../../components/Routes/UnAuthRoute/UnAuthRoute';

const mapStateToProps = ({
	userState: { userInfo: { token } },
}) => ({ token });

export default connect(mapStateToProps)(UnAuthRoute);