import React from 'react';
import thunk from 'redux-thunk';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

import AppContainer from './containers/AppContainer';
import globalReducer from './reducers/globalReducer';

import './index.css';

const devTools = window.devToolsExtension ? window.devToolsExtension() : f => f;
const store = createStore(globalReducer, compose(applyMiddleware(thunk), devTools));

render(
  <Provider store={store}>
		<AppContainer />
	</Provider>,
  document.getElementById('root')
);
