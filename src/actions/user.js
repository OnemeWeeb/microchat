import { ACTIONS } from '../utils/constants';
import { signIn, signUp, checkToken } from '../requests/user';
import { getErrorList } from '../utils/form';

export const fillUserInfo = (payload) => ({
	type: ACTIONS.FILL_USER_INFO,
	payload,
});

export const clearUserInfo = () => ({
	type: ACTIONS.CLEAR_USER_INFO,
});

export const signInAction = (payload) => (dispatch, getState) => {
	const { values, history, ref } = payload;
	console.log(ref);
	signIn(values)
		.then(res => {
			dispatch(fillUserInfo({
				...getState().userState.userInfo,
				email: res.data.resultData.email,
				token: res.data.resultData.tokens.accessToken,
			}));
			localStorage.setItem('token', res.data.resultData.tokens.accessToken);
		})
		.then(() => history.push('/chat/'))
		.catch((error) => {
			const { errorMessage, errors } = error.response.data;

			if (errors.length) {
				ref.setErrors(getErrorList(ref.values, errors.map(item => item.field)));
			} else {
				ref.setFieldError('api', errorMessage);
			}
		});
}

export const signUpAction = (payload) => () => {
	const { values, history, ref } = payload;
	signUp(values)
		.then(() => history.push('/login/'))
		.catch((error) => {
			const { errorMessage, errors } = error.response.data;

			if (errors.length) {
				ref.setErrors(getErrorList(ref.values, errors.map(item => item.field)));
			} else {
				ref.setFieldError('api', errorMessage);
			}
		});
}

export const checkTokenAction = () => (dispatch, getState) => {
	checkToken()
		.then(res => dispatch(fillUserInfo({
			...getState().userState.userInfo,
			name: res.data.resultData.name,
		})))
		.catch(() => dispatch(clearUserInfo()))
}