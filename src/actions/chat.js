import { ACTIONS } from '../utils/constants';

export const fillMessageList = (payload) => ({
	type: ACTIONS.FILL_MESSAGE_LIST,
	payload,
});

export const clearMessageList = () => ({
	type: ACTIONS.CLEAR_MESSAGE_LIST,
});