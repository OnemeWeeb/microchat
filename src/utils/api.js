import axios from 'axios';

class Api {
	initApi() {
		axios.defaults.headers.common['Authorization'] = localStorage.getItem('token');

		return axios;
	}

	get axios() {
		return this.initApi();
	}
}

const api = new Api();

export default api;
