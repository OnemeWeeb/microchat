import io from 'socket.io-client';
import { DOMAIN } from '../utils/constants';
let socket;

export const initiateSocket = () => {
  socket = io(DOMAIN, { 
		query: {
			Authorization: localStorage.getItem('token') || '',
		}
	});
  
  if (socket) socket.emit('join');
}

export const disconnectSocket = () => {
  
  if (socket) socket.disconnect();
}

export const subscribeToChat = (fillMessageList) => {
  if (!socket) return (true);
  socket.on('message', message => {
		fillMessageList({
			user: message.username,
			text: message.text,
			date: new Date(message.createdAt),
		});
    
    return message;
  });
}

export const sendMessage = (message) => {
  if (socket) socket.emit('sendMessage', message.trim());
}