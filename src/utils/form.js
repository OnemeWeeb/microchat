/* eslint-disable default-case */
export const getError = ({ name, value, password }) => {
	let error = '';

	if (value === '') {
		error = 'The field is not filled!';
	} else {
		switch (name) {
			case 'password':
				if (value.length < 6) {
					error = 'Password must be more than 6 characters!';
				}
				break;
			case 'confirmPassword':
				if (value !== password) {
					error = 'The entered passwords do not match!';
				}
				break;
			case 'nickname':
				if (/^[a-zA-Z0-9\s]+$/.test(value) === false) {
					error = 'Username can only contain EN!';
				}
				break;
			case 'email':
				if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value) === false) {
					error = 'Invalid email format!';
				}
				break;
		}
	}

	return error;
};

export const getErrorList = (values, errors) => {
	const errorList = {};

	errors.forEach((item) => {
		item = item === 'name' ? 'nickname' : item;
		errorList[item] = getError({
			name: item,
			value: values[item],
			password: values.password,
		});
	});

	const resultErrorList = {
		...errorList,
	}

	return Object.values(resultErrorList).filter(item => item !== '').length ? resultErrorList : {};
}

export const getProps = (name, component) => {
	let type = '';

	switch (name) {
		case 'nikname':
			type = 'text';
			break;
		case 'email':
			type = 'email';
			break;
		case 'age':
			type = 'number';
			break;
		case 'password':
		case 'confirmPassword':
			type = 'password';
			break;
	}

	return {
		id: name,
		name,
		type,
		component,
	}
}
