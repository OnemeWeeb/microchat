export const getDate = (date) => `${date.getHours()}:${date.getMinutes().toString().length < 2 ? `0${date.getMinutes()}` : date.getMinutes()}`;

export const getType = (userName, senderName) => {
	console.log(userName);
	switch (senderName) {
		case 'Admin':
			return {
				message: 'system',
				alert: 'alert-success',
			};
		case `${userName}`:
			return {
				message: 'mine',
				alert: 'alert-primary',
			};
		default:
			return {
				message: 'other',
				alert: 'alert-secondary'
			};
	}
}
