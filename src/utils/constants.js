export const DOMAIN = `http://${process.env.REACT_APP_HOST}:${process.env.REACT_APP_PORT}`;

export const URL = {
	AUTHORIZATION: `${DOMAIN}/api/auth/token`,
	REGISTRATION: `${DOMAIN}/api/users`,
	CHECK_TOKEN: `${DOMAIN}/api/profile/me`,
};

export const ROUTES = {
	AUTHORIZATION: '/login/',
	REGISTRATION: '/registration/',
	CHAT: '/chat/',
};

export const ACTIONS = {
	FILL_USER_INFO: 'FILL_USER_INFO',
	GET_STATUS_VALUE: 'GET_STATUS_VALUE',
	CLEAR_USER_INFO: 'CLEAR_USER_INFO',
	FILL_MESSAGE_LIST: 'FILL_MESSAGE_LIST',
	CLEAR_MESSAGE_LIST: 'CLEAR_MESSAGE_LIST',
};

export const VALUES = {
	AUTHORIZATION: {
		email: '',
		password: '',
	},
	REGISTRATION: {
		email: '',
		nickname: '',
		age: '',
		password: '',
		confirmPassword: '',
	},
	CHAT: {
		value: '',
		isShiftHeld: false,
	}
};