import React, { useState } from 'react';
import TextField from '../TextField/TextField';
import './PasswordField.css';

const PasswordField = ({ name, field }) => {
  const initialState = {
    isShowing: false,
  }

  const [state, setState] = useState(initialState);

  return (
    <div className='password_field'>
			<TextField
				name={name} 
				type={state.isShowing ? 'text' : 'password'}
        max={30}
        field={field}
			/>
      <div className={`visibility ${state.isShowing ? 'show' : 'hide' }`} onClick={() => setState({ isShowing: !state.isShowing })} />
    </div>
  );
}

export default PasswordField;