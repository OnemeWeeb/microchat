import React from 'react';

const TextField = ({ name, type, max, autofocus, field }) => (
	<input
		name={name}
		type={type}
		className="form-control"
		maxLength={max}
		autoFocus={autofocus}
		{...field}
	/>
);


export default TextField;