import React from 'react';

import { getDate } from '../../../utils/chat';

import './Message.css';

const Message = ({ message, type }) => (
	<div className={`message ${type.message}`}>
		{(type.message !== 'system' && (type.message !== 'mine')) && <div className="header">{message.user}</div>}
		<div className={`body ${type.message}_message_body`}>
			{(type.message !== 'system') && <div className={`date ${type.message}_message_date`}>
				{getDate(message.date)}
			</div>}
			<div className={`text alert ${type.alert} ${type.message}_message_text`}>
				{message.text}
			</div>
		</div>
	</div>
);

export default Message;