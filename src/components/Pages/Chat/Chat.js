import React, { useState, useEffect, useRef } from 'react';

import Message from '../../../containers/Common/MessageContainer';

import { VALUES } from '../../../utils/constants';
import { getType } from '../../../utils/chat'
import { initiateSocket, disconnectSocket, subscribeToChat, sendMessage } from '../../../utils/connection';

import './Chat.css';

const Chat = ({ fillMessageList, clearMessageList, checkToken, clearUserInfo, messages, history, name }) => {
	const [state, setState] = useState(VALUES.CHAT);
	const buttonRef = useRef(null);
	const listRef = useRef(null);

	useEffect(() => {
		initiateSocket();
		subscribeToChat(fillMessageList);

		return () => disconnectSocket();
	}, []);

	useEffect(() => {
		listRef.current.scrollTop = listRef.current.scrollHeight;
		(name === '') && checkToken();
	}, [messages])

	const onSubmit = () => {
		sendMessage(state.value);
		setState(VALUES.CHAT);
	}

	const onKeyDown = (e) => {
		const { key } = e;

		if (key === 'Shift') {
			setState({ ...state, isShiftHeld: true });
		}

		if (key === 'Enter' && !state.isShiftHeld) {
			e.preventDefault();
			onSubmit();
		}
	}

	const signOut = () => {
		clearMessageList();
		clearUserInfo();
		history.push('/login/');
	}

	const resetShiftHeldState = ({ key }) => {
		if (key === 'Shift') {
			setState({ ...state, isShiftHeld: false });
		}
	}

	return (
		<div className='chat'>
			<div className="left_bar">
				<button
					className="btn btn-secondary btn-lg logout"
					ref={buttonRef}
					type='logout'
					onClick={() => {
						disconnectSocket();
						signOut();
					}}
				>
					Log Out
				</button>
			</div>
			<div className="main">
				<div className="message_list" ref={listRef}>
					{messages.map((item, index) => <Message key={index} index={index} message={item} type={getType(name, item.user)}/>)}
				</div>
				<div className="input input-group-sm">
					<textarea
						value={state.value}
						placeholder="Message"
						className="form-control field"
						onChange={(e) => setState({
							...state,
							value: e.target.value,
						})}
						onKeyDown={(e) => onKeyDown(e)}
						onKeyUp={resetShiftHeldState}
					/>
					<button className="btn btn-primary btn-lg" type='submit' onClick={() => onSubmit()}>Send</button>
				</div>
			</div>
		</div>
	);

};

export default Chat;