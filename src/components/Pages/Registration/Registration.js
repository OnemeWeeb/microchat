import React, { useRef } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { Link } from 'react-router-dom';

import TextField from '../../Common/TextField/TextField';
import PasswordField from '../../Common/PasswordField/PasswordField';

import { getErrorList, getProps } from '../../../utils/form';
import { VALUES } from '../../../utils/constants';

import './Registration.css';

const Registration = ({ history, signUp }) => {
	const formRef = useRef()

	return (
		<div className='login'>
			<h2 className='header'>Sign Up</h2>
			<Formik
				innerRef={formRef}
				initialValues={VALUES.REGISTRATION}
				validate={values => getErrorList(values, Object.keys(values))}
				onSubmit={(values) => {
					signUp({
						values: {
							email: values.email,
							password: values.password,
							name: values.nickname,
							age: values.age,
						},
						history,
						ref: formRef.current,
					})
				}}>
				{({ values, errors }) => (
					<Form className='form'>
						{errors.api && <div className="error">{errors.api}</div>}
						{Object.keys(values).map((item, index) =>
							<div key={index} className='form-group'>
								<label htmlFor={item}>{item[0].toUpperCase() + item.substring(1)}</label>
								<Field {...getProps(item, (item.includes('password') || item.includes('confirmPassword')) ? PasswordField : TextField)} />
								<ErrorMessage name={item} component='div' />
							</div>
						)}
						<Link className='link' to='/login/'>Уже есть аккаунт?</Link>
						<button className='btn btn-primary' type='submit'>Sign Up</button>
					</Form>
				)}
			</Formik>
		</div>
	);
}

export default Registration;