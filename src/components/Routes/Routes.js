import React from 'react';
import { createBrowserHistory } from 'history';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Login from '../../containers/Pages/SignIn/SignInContainer';
import Registration from '../../containers/Pages/SignUp/SignUpContainer';
import Chat from '../../containers/Pages/Chat/ChatContainer';
import AuthRoute from '../../containers/Routes/AuthRoute';
import UnAuthRoute from '../../containers/Routes/UnAuthRoute';

import { ROUTES } from '../../utils/constants';

const Routes = () => (
  <BrowserRouter history={createBrowserHistory()}>
    <Switch>
      <Route exact path="/" render={() => <Redirect to={ROUTES.AUTHORIZATION} />} />
      <UnAuthRoute path={ROUTES.AUTHORIZATION} exact component={Login} />
      <UnAuthRoute path={ROUTES.REGISTRATION} exact component={Registration} />
      <AuthRoute path={ROUTES.CHAT} exact component={Chat} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
