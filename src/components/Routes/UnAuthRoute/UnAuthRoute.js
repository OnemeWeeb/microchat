import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { ROUTES } from '../../../utils/constants';

const UnAuthRoute = ({ token, component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (!token ? <Component {...props} /> : <Redirect to={ROUTES.CHAT} />)}
  />
);

export default UnAuthRoute;
