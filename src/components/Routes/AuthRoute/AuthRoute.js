import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { ROUTES } from '../../../utils/constants';

const AuthRoute = ({ token, component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (token ? <Component {...props} /> : <Redirect to={ROUTES.AUTHORIZATION} />)}
  />
);

export default AuthRoute;

