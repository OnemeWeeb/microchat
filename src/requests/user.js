import { URL } from '../utils/constants';
import api from '../utils/api';

export const signUp = (values) => api.axios
	.post(URL.REGISTRATION,
		values,
		{
			headers: {
				'Content-Type': 'application/json',
			},
		});

export const signIn = (values) => api.axios
	.post(URL.AUTHORIZATION,
		values,
		{
			headers: {
				'Content-Type': 'application/json',
			},
		}
	)

export const checkToken = () => api.axios.get(URL.CHECK_TOKEN);